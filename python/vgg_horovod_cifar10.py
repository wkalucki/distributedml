import os
import keras
from keras.datasets import cifar10
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, Dense, Dropout, Activation, Flatten, BatchNormalization
from keras.preprocessing.image import ImageDataGenerator
import tensorflow as tf
import horovod.keras as hvd
from keras import backend as K, regularizers
import matplotlib.pyplot as plt

os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

# init Horovod
hvd.init()

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
config.gpu_options.visible_device_list = str(hvd.local_rank())
K.set_session(tf.Session(config=config))

(x_train, y_train), (x_test, y_test) = cifar10.load_data()

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255

batch_size = 128
num_classes = 10
epochs = 3
iterations = 50000
weight_decay = 1e-4

y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

model = Sequential()

# First conv layer

model.add(Conv2D(32, (3, 3), padding='same', kernel_regularizer=regularizers.l2(weight_decay), input_shape=x_train.shape[1:]))
model.add(Activation('relu'))
model.add(BatchNormalization())

# Second conv layer

model.add(Conv2D(32, (3, 3), padding='same', kernel_regularizer=regularizers.l2(weight_decay)))
model.add(Activation('relu'))
model.add(BatchNormalization())

# 1st Polling layer
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.2))

# Third conv layer
model.add(Conv2D(64, (3, 3), padding='same', kernel_regularizer=regularizers.l2(weight_decay)))
model.add(Activation('relu'))
model.add(BatchNormalization())

# Fourth conv layer
model.add(Conv2D(64, (3, 3), padding='same', kernel_regularizer=regularizers.l2(weight_decay)))
model.add(Activation('relu'))
model.add(BatchNormalization())

# 2nd Polling layer
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.3))

# Third conv layer
model.add(Conv2D(128, (3, 3), padding='same', kernel_regularizer=regularizers.l2(weight_decay)))
model.add(Activation('relu'))
model.add(BatchNormalization())

# Fourth conv layer
model.add(Conv2D(128, (3, 3), padding='same', kernel_regularizer=regularizers.l2(weight_decay)))
model.add(Activation('relu'))
model.add(BatchNormalization())

# 2nd Polling layer
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.4))

model.add(Flatten())

# Two dense layers
model.add(Dense(512))
model.add(Activation('relu'))
model.add(Dropout(0.4))

# model.add(Dense(512))
# model.add(Activation('relu'))
# model.add(Dropout(0.5))

# Classification layer
model.add(Dense(num_classes))
model.add(Activation('softmax'))

# Horovod optimizer wrapped around keras RMSprop optimizer
opt = keras.optimizers.rmsprop(lr=0.0001, decay=1e-6)
opt = hvd.DistributedOptimizer(opt)

image_generator = ImageDataGenerator(rotation_range=90,
                                     width_shift_range=0.1, height_shift_range=0.1,
                                     horizontal_flip=True)
image_generator.fit(x_train)

model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])

# history = model.fit_generator(image_generator.flow(x_train, y_train, batch_size=batch_size),
#                               steps_per_epoch=x_train.shape[0] // batch_size, epochs=epochs,shuffle=True,
#                               verbose=1, validation_data=(x_test, y_test))

# save to disk
#
history = model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, shuffle=True, verbose=1,
                    validation_data=(x_test, y_test))

plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.savefig('/plots/accuracy.png')
# summarize history for loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.savefig('/plots/modelLoss.png')
