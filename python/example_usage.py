import horovod.keras as hvd
import tensorflow as tf
from keras import backend as K

# Necessary imports for Keras Horovod training

# Initialize Horovod
hvd.init()

# Horovod: pin GPU to be used to process local rank (one GPU per process)
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
config.gpu_options.visible_device_list = str(hvd.local_rank())
K.set_session(tf.Session(config=config))

# Load dataset
(x_train, y_train), (x_test, y_test) = ...

# Build desired model
model = ...

# Choose the optimizer and apply linear scaling rule based on Horovod size - number of processes
opt = keras.optimizers.sgd(lr=lr * hvd.size(), momentum=0.9, decay=0.0001)

# Wrap it with Horovod Distributed Optimizer
opt = hvd.DistributedOptimizer(opt)

#  BroadcastGlobalVariablesCallback is required when starting the training from checkpoint or
#  initializing the layers with random values.This callback ensures the consistent initialization
#  of all worker nodes
#  MetricAverageCallback efficiently averages metrics across worker nodes

# LearningRateWarmupCallback is optional callback allowing for gradual learning rate warm-up
# for the specified amounts of epochs across worker nodes
callbacks = [
    hvd.callbacks.BroadcastGlobalVariablesCallback(0),
    hvd.callbacks.MetricAverageCallback(),
    hvd.callbacks.LearningRateWarmupCallback(warmup_epochs=5, verbose=1),
    keras.callbacks.ReduceLROnPlateau(patience=10, verbose=1, min_lr=0.5e-6),
    lr_scheduler
]

# Compile the model
model.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=opt,
              metrics=['accuracy'])

# Configure the image generator
image_generator = ...
image_generator.fit(x_train)




# Perform the synchronous All-reduce training , reducing the the performed steps amount
# based on hvd.size()
history = model.fit_generator(image_generator.flow(x_train, y_train, batch_size=batch_size),
                              steps_per_epoch=(x_train.shape[0] // batch_size) // hvd.size(), epochs=epochs,
                              callbacks=callbacks,
                              shuffle=True,
                              verbose=verbose, validation_data=(x_test, y_test),
                              validation_steps=((x_train.shape[0] // batch_size) // hvd.size()))

# Evaluate the model
score = hvd.allreduce(model.evaluate(x_test, y_test, verbose=0))
print('Test loss:', score[0])
print('Test accuracy:', score[1])
