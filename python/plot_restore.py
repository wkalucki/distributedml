import cloudpickle
import horovod.keras as hvd
import keras
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from keras import backend as K, Model
from keras import regularizers
from keras.callbacks import LearningRateScheduler
from keras.datasets import cifar100
from keras.layers import Conv2D, Activation, BatchNormalization, AveragePooling2D, Flatten, Dense
from keras.layers import (
    Input
)
from keras.layers.merge import add
from keras.preprocessing.image import ImageDataGenerator
import time


def restore(filepath):
    with open(filepath, "rb") as file:
        history = cloudpickle.load(file)
        accuracy = plt.figure(0)
        plt.plot(history.history['acc'][:120])
        plt.plot(history.history['val_acc'][:120])
        plt.title('model accuracy')
        plt.ylabel('accuracy')
        plt.xlabel('epoch')
        plt.legend(['train', 'test'], loc='upper left')
        plt.savefig('plots/accuracy' + '_test.png')
        # plt.show()
        # summarize history for loss
        model_loss = plt.figure()
        plt.plot(history.history['loss'][:120])
        plt.plot(history.history['val_loss'][:120])
        plt.title('model loss')
        plt.ylabel('loss')
        plt.xlabel('epoch')
        plt.legend(['train', 'test'], loc='upper left')
        plt.savefig('plots/modelLoss' + 'test_.png')
        # plt.show()


if __name__ == '__main__':
    restore('/home/wojciech/licencjat/distributedml/plots/historyCifar10_batch128_2019-06-09-08-12.pkl')
