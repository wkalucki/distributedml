import os
import keras
from keras.datasets import cifar10
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, Dense, Dropout, Activation, Flatten, BatchNormalization
from keras.preprocessing.image import ImageDataGenerator
import tensorflow as tf
import horovod.keras as hvd
from keras import backend as K, regularizers
import matplotlib.pyplot as plt

os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

# init Horovod
hvd.init()

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
config.gpu_options.visible_device_list = str(hvd.local_rank())
K.set_session(tf.Session(config=config))

# TODO read datasets from HDFS
(x_train, y_train), (x_test, y_test) = cifar10.load_data()

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255

batch_size = 128
num_classes = 10
epochs = 125
iterations = 50000
weight_decay = 1e-4

y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)

model = Sequential()

# First conv layer

model.add(Conv2D(32, (3, 3), padding='same', kernel_regularizer=regularizers.l2(weight_decay), input_shape=x_train.shape[1:]))
model.add(Activation('relu'))
model.add(BatchNormalization())

# Second conv layer

model.add(Conv2D(32, (3, 3), padding='same', kernel_regularizer=regularizers.l2(weight_decay)))
model.add(Activation('relu'))
model.add(BatchNormalization())

# 1st Polling layer
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.2))

# Third conv layer
model.add(Conv2D(64, (3, 3), padding='same', kernel_regularizer=regularizers.l2(weight_decay)))
model.add(Activation('relu'))
model.add(BatchNormalization())

# Fourth conv layer
model.add(Conv2D(64, (3, 3), padding='same', kernel_regularizer=regularizers.l2(weight_decay)))
model.add(Activation('relu'))
model.add(BatchNormalization())

# 2nd Polling layer
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.3))

# Third conv layer
model.add(Conv2D(128, (3, 3), padding='same', kernel_regularizer=regularizers.l2(weight_decay)))
model.add(Activation('relu'))
model.add(BatchNormalization())

# Fourth conv layer
model.add(Conv2D(128, (3, 3), padding='same', kernel_regularizer=regularizers.l2(weight_decay)))
model.add(Activation('relu'))
model.add(BatchNormalization())

# 2nd Polling layer
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.4))

model.add(Flatten())

# Two dense layers
model.add(Dense(512))
model.add(Activation('relu'))
model.add(Dropout(0.4))

# model.add(Dense(512))
# model.add(Activation('relu'))
# model.add(Dropout(0.5))

# Classification layer
model.add(Dense(num_classes))
model.add(Activation('softmax'))

# Horovod optimizer wrapped around keras RMSprop optimizer
opt = keras.optimizers.rmsprop(lr=0.0003, decay=1e-6)
opt = hvd.DistributedOptimizer(opt)

image_generator = ImageDataGenerator(rotation_range=90,
                                     width_shift_range=0.1, height_shift_range=0.1,
                                     horizontal_flip=True)
image_generator.fit(x_train)

model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])

verbose = 1 if hvd.rank == 0 else 0

callbacks = [
    # Horovod: broadcast initial variable states from rank 0 to all other processes.
    # This is necessary to ensure consistent initialization of all workers when
    # training is started with random weights or restored from a checkpoint.
    hvd.callbacks.BroadcastGlobalVariablesCallback(0),

    # Horovod: average metrics among workers at the end of every epoch.
    #
    # Note: This callback must be in the list before the ReduceLROnPlateau,
    # TensorBoard or other metrics-based callbacks.
    hvd.callbacks.MetricAverageCallback(),

    # Horovod: using `lr = 1.0 * hvd.size()` from the very beginning leads to worse final
    # accuracy. Scale the learning rate `lr = 1.0` ---> `lr = 1.0 * hvd.size()` during
    # the first five epochs. See https://arxiv.org/abs/1706.02677 for details.
    # hvd.callbacks.LearningRateWarmupCallback(warmup_epochs=5, verbose=1),

    # Reduce the learning rate if training plateaues.
    # keras.callbacks.ReduceLROnPlateau(patience=10, verbose=1),
]

# # Horovod: save checkpoints only on worker 0 to prevent other workers from corrupting them.
# if hvd.rank() == 0:
#     callbacks.append(keras.callbacks.ModelCheckpoint('./checkpoint-{epoch}.h5'))

history = model.fit_generator(image_generator.flow(x_train, y_train, batch_size=batch_size),
                              steps_per_epoch=(x_train.shape[0] // batch_size) // hvd.size(), epochs=epochs,
                              shuffle=True,
                              verbose=verbose, validation_data=(x_test, y_test),
                              validation_steps=3 * ((x_train.shape[0] // batch_size) // hvd.size()),
                              callbacks=callbacks)

# save to disk
#
# history = model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, shuffle=True, verbose=1,
#                     validation_data=(x_test, y_test))

# plt.plot(history.history['acc'])
# plt.plot(history.history['val_acc'])
# plt.title('model accuracy')
# plt.ylabel('accuracy')
# plt.xlabel('epoch')
# plt.legend(['train', 'test'], loc='upper left')
# plt.savefig('/plots/accuracy.png')
# # summarize history for loss
# plt.plot(history.history['loss'])
# plt.plot(history.history['val_loss'])
# plt.title('model loss')
# plt.ylabel('loss')
# plt.xlabel('epoch')
# plt.legend(['train', 'test'], loc='upper left')
# plt.savefig('/plots/modelLoss.png')
score = hvd.allreduce(model.evaluate(x_test, y_test, verbose=0))
print('Test loss:', score[0])
print('Test accuracy:', score[1])