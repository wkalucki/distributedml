import os
import numpy as np
import horovod.keras as hvd
import keras
import tensorflow as tf
from keras import backend as K, Model
from keras.callbacks import LearningRateScheduler
from keras.datasets import cifar100
from keras.layers import Conv2D, Activation, BatchNormalization, AveragePooling2D, Flatten, Dense
from keras.layers import (
    Input
)
from keras import regularizers
from keras.utils import plot_model
from keras.layers.merge import add
from keras.preprocessing.image import ImageDataGenerator
import matplotlib.pyplot as plt

# os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

# init Horovod
hvd.init()

config = tf.ConfigProto()
config.gpu_options.allow_growth = True
config.gpu_options.visible_device_list = str(hvd.local_rank())
K.set_session(tf.Session(config=config))

# TODO read datasets from HDFS
(x_train, y_train), (x_test, y_test) = cifar100.load_data()

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255
mean_image = np.mean(x_train, axis=0)
x_train -= mean_image
x_test -= mean_image

batch_size = 128
num_classes = 100
epochs = 200
iterations = 50000
weight_decay = 1e-4

y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)


def build_resnet(input_shape, filter_base, block_function, layers):
    print(input_shape)
    print("Building resnet with layers" + str(layers))
    input_layer = Input(shape=input_shape)
    filters = filter_base

    starting_layer = Conv2D(filters=filter_base, kernel_size=3, strides=(1, 1), padding='same')(input_layer)
    starting_layer = BatchNormalization()(starting_layer)
    starting_layer = Activation("relu")(starting_layer)
    x = starting_layer
    for index, value in enumerate(layers):
        for repetition in range(value):
            stride = 1
            if repetition == 0 and not index == 0:
                stride = 2

            x = build_block(block_function, x, filters, stride,
                            is_first_block_first_layer=(index == 0 and repetition == 0))
            print("Builded block " + str(repetition) + " in layer " + str(index))
        filters *= 2
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = AveragePooling2D(pool_size=8)(x)
    y = Flatten()(x)
    outputs = Dense(num_classes,
                    activation='softmax',
                    kernel_initializer='he_normal')(y)

    return Model(inputs=input_layer, outputs=outputs)


def resnet_layer(input, strides=1, filters=16, kernel_size=3, activation='relu', batch_norm=True):
    if batch_norm:
        input = BatchNormalization()(input)
    if activation is not None:
        input = Activation(activation)(input)
    conv = Conv2D(filters=filters, kernel_size=kernel_size, strides=strides, padding='same',
                  kernel_initializer='he_normal', kernel_regularizer=regularizers.l2(weight_decay))(input)
    return conv


def build_block(block_function, input, filters, stride, is_first_block_first_layer=False):
    print('Building block with filter {}  stride {}'.format(filters, stride))
    return block_function(input, filters, stride, is_first_block_first_layer)


def basic_block(input, filters, stride, is_first_block_first_layer):
    if is_first_block_first_layer:
        print("First layer omitting batch and relu")
        block = resnet_layer(input=input, filters=filters, strides=stride, batch_norm=False, activation=None)
    else:
        block = resnet_layer(input=input, filters=filters, strides=stride)
    block = resnet_layer(input=block, filters=filters)
    return skip_connection(input, block)


def bottleneck_block(input, filters, stride, is_first_block_first_layer):
    bottleneck_out_filter = filters * 4
    if is_first_block_first_layer:
        block = resnet_layer(input=input, filters=filters, strides=stride, kernel_size=1, batch_norm=False,
                             activation=None)
    else:
        block = resnet_layer(input=input, filters=filters, strides=stride, kernel_size=1)

    block = resnet_layer(input=block, filters=filters, kernel_size=3)
    block = resnet_layer(input=block, filters=bottleneck_out_filter)
    return block


def skip_connection(input, residual):
    ROW_AXIS = 1
    COL_AXIS = 2
    CHANNEL_AXIS = 3
    input_shape = K.int_shape(input)
    residual_shape = K.int_shape(residual)
    stride_width = int(round(input_shape[ROW_AXIS] / residual_shape[ROW_AXIS]))
    stride_height = int(round(input_shape[COL_AXIS] / residual_shape[COL_AXIS]))
    equal_channels = input_shape[CHANNEL_AXIS] == residual_shape[CHANNEL_AXIS]

    shortcut = input
    # 1 X 1 conv if shape is different. Else identity.
    if stride_width > 1 or stride_height > 1 or not equal_channels:
        shortcut = Conv2D(filters=residual_shape[CHANNEL_AXIS],
                          kernel_size=(1, 1),
                          strides=(stride_width, stride_height),
                          padding="valid",
                          kernel_initializer="he_normal",
                          kernel_regularizer=regularizers.l2(weight_decay))(input)

    return add([shortcut, residual])


model = build_resnet(input_shape=x_train.shape[1:], filter_base=16, block_function=basic_block, layers=[2, 2, 2])
model.summary()
# plot_model(model, to_file='modelCifar100.png')
opt = keras.optimizers.sgd(lr=0.1, momentum=0.9, decay=0.0001)
opt = hvd.DistributedOptimizer(opt)
model.compile(loss='categorical_crossentropy',
              optimizer=opt,
              metrics=['accuracy'])
image_generator = ImageDataGenerator(
    featurewise_center=False,  # set input mean to 0 over the dataset
    samplewise_center=False,  # set each sample mean to 0
    featurewise_std_normalization=False,  # divide inputs by std of the dataset
    samplewise_std_normalization=False,  # divide each input by its std
    zca_whitening=False,  # apply ZCA whitening
    rotation_range=0,  # randomly rotate images in the range (degrees, 0 to 180)
    width_shift_range=0.1,  # randomly shift images horizontally (fraction of total width)
    height_shift_range=0.1,  # randomly shift images vertically (fraction of total height)
    horizontal_flip=True,  # randomly flip images
    vertical_flip=False)  # randomly flip images


def lr_schedule(epoch):
    lr = 0.1
    if epoch > 180:
        lr *= 0.5e-3
    elif epoch > 160:
        lr *= 1e-3
    elif epoch > 120:
        lr *= 1e-2
    elif epoch > 80:
        lr *= 1e-1
    return lr


lr_scheduler = LearningRateScheduler(lr_schedule, verbose=1)

callbacks = [
    hvd.callbacks.LearningRateWarmupCallback(warmup_epochs=5, verbose=1),
    keras.callbacks.ReduceLROnPlateau(patience=10, verbose=1, min_lr=0.5e-6),
    lr_scheduler
]

image_generator.fit(x_train)

history = model.fit_generator(image_generator.flow(x_train, y_train, batch_size=batch_size),
                              steps_per_epoch=(x_train.shape[0] // batch_size) // hvd.size(), epochs=epochs,
                              callbacks=callbacks,
                              shuffle=True,
                              verbose=1, validation_data=(x_test, y_test),
                              validation_steps=((x_train.shape[0] // batch_size) // hvd.size()))

score = hvd.allreduce(model.evaluate(x_test, y_test, verbose=0))
print('Test loss:', score[0])
print('Test accuracy:', score[1])

# summarize history for accuracy
accuracy = plt.figure(0)
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.savefig('accuracyCifar10v2.png')
# summarize history for loss
model_loss = plt.figure()
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.savefig('modelLossCifar10v2.png')

# TODO lr schelude
# todo warmup
# todo find best optimizer
