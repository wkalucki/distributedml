#DistributedML
This project was created as a part of ongoing bachelor's thesis paper on distributed machine learning technologies and their applications.
###Environment Setups
Prerequisites :
* Linux operating system
* [CUDA compatible CPU](https://developer.nvidia.com/cuda-gpus) <br>

Following setup guide is meant for UBUNTU distribution however other Linux distributions installation process should be relatively the same.
1. [Install docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
2. [Install docker compose](https://docs.docker.com/compose/install/)
3. Install Nvidia Drivers for your GPU and reboot your computer
4. [Install Nvidia-Docker](https://github.com/NVIDIA/nvidia-docker)
5. Launch runHorovod.sh script to verify successful environment setup

Optional steps:
* [Setup user group for docker to run docker commands without sudo](https://docs.docker.com/install/linux/linux-postinstall/)
* [Change default storing location of docker images from /var/lib/docker to specified path ](https://linuxconfig.org/how-to-move-docker-s-default-var-lib-docker-to-another-directory-on-ubuntu-debian-linux)
###Running horovod jobs
[Make sure you have met this project prerequisites and went through installation process](#environment-setups)
####Standalone multiple GPU
Your python scripts should be located in /python folder.During docker container creation mentioned scripts will be copied to container to working dir /scripts
<br>
<br>
In order to run specific python script launch runHorovod.sh script passing python script name as argument

**Example**
<br>
./runHorovod.sh keras_cifar_10.py

####Running distributed on multiple computers
* [Setup passwordless SSH connections between hosts](http://www.linuxproblem.org/art_9.html)
* On the main worker run:
```bash 
nvidia-docker run -it --network=host -v /distributedml/ssh:/root/.ssh horovod:latest
``` 
Where /ssh should be folder containing `id_rsa` ,`authorized_keys` pair
<br>

* On the remaining workers run :
```bash
nvidia-docker run -it --network=host -v /distributedml/ssh:/root/.ssh horovod:latest \
    bash -c "/usr/sbin/sshd -p 12345; sleep infinity"
``` 
You can also change the 12345 port to your custom selected port
* After you have launched all the remaining workers ,from the main worker run following command
```bash
horovodrun -np 16 --verbose -H main_host:4,host2:4,host3:4,host4:4 -p 12345 python <your python script>

```
**Parameters :**
1. -np  describes how many processes to launch (keep in mind each process is pinned to one GPU/CPU socket)
2. -H  listing hosts on which the operations will be performed
3. host:number syntax for specifying how many processes to run on each worker host
4. -p port on which to perform the horovod job (must be the same as the port provided on worker hosts in step two)
   

###References
1. [Horovod repository](https://github.com/horovod/horovod)
2. [Nvidia-Docker repository](https://github.com/NVIDIA/nvidia-docker)