#!/usr/bin/env bash


dockerType="docker"
if [[ $1 == "GPU" ]];
    then
        dockerType="nvidia-docker"
fi
portVar="$1"
cat > run_secondary_worker.sh << EOF
#!/usr/bin/env bash
if [ -z "\$1" ];
    then
        echo Please provide the desired port;exit 1
fi
echo "Secondary worker launched"
$dockerType run -it --network=host -v $(pwd)/ssh:/root/.ssh  -v $(pwd)/python:/scripts  horovod:latest \
    bash -c "/usr/sbin/sshd -p "\$1"; sleep infinity"

#cat /etc/hostname
EOF
cat > run_primary_worker.sh << EOF
#!/usr/bin/env bash
$dockerType run -it --network=host -v $(pwd)/ssh:/root/.ssh -v $(pwd)/plots/:/scripts/plots -v $(pwd)/python:/scripts horovod:latest

#cat /etc/hostname
EOF
chmod +x run_secondary_worker.sh
chmod +x run_primary_worker.sh
docker build -t horovod:latest --build-arg runtime=$1 .

