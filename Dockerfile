FROM horovod/horovod:0.16.1-tf1.12.0-torch1.0.0-mxnet1.4.0-py3.5 

RUN apt-get update
RUN pip install matplotlib
RUN apt-get install -y graphviz
RUN pip install graphviz
RUN pip install pydot
RUN apt-get install nano
WORKDIR /scripts
RUN mkdir plots
ARG runtime
RUN if [ "$runtime" = "CPU" ] ; then ldconfig /usr/local/cuda/lib64/stubs ;  fi

