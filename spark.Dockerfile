FROM horovod/horovod:0.16.1-tf1.12.0-torch1.0.0-mxnet1.4.0-py3.5


WORKDIR /spark
ARG SPARK_VERSION="spark-2.4.3-bin-hadoop2.7"
ARG file="${SPARK_VERSION}.tgz"
RUN apt-get update && apt-get install -y  openjdk-8-jre-headless  && apt-get install -y  nano
RUN curl -O http://ftp.man.poznan.pl/apache/spark/spark-2.4.3/${file} && tar zxvf ${file} && cd ${SPARK_VERSION}

RUN pip install findspark
RUN pip install matplotlib
RUN ln -s /spark/spark-2.4.3-bin-hadoop2.7 /spark/home

WORKDIR /spark/home

ENV SPARK_HOME  "/spark/home"
ENV SPARK_MASTER_HOST "192.168.1.101"
ENV SPARK_MASTER "spark://${SPARK_MASTER_HOST}:7077"
ENV JAVA_HOME "/usr/lib/jvm/java-8-openjdk-amd64"
#ENV PYSPARK_PYTHON "python3"
RUN export PATH=$SPARK_HOME/bin:$PATH
EXPOSE 4040:4040
EXPOSE 7077:7077
EXPOSE 8080:8080
EXPOSE 8081:8081

RUN ldconfig /usr/local/cuda/lib64/stubs
RUN apt-get install -y rsync
WORKDIR /scripts
COPY /python /scripts
